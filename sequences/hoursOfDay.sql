with 
  recursive 
  hoursOfDay(hours) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select hoursOfDay.hours + 1
    from hoursOfDay
    where hoursOfDay.hours < 23 -- finish
  )
select hoursOfDay.hours 
from hoursOfDay
--where hoursOfDay.hours in (10, 22) 
order by hoursOfDay.hours