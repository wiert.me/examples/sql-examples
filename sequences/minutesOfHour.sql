with 
  recursive 
  minutesOfHour(minutes) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select minutesOfHour.minutes + 1
    from minutesOfHour
    where minutesOfHour.minutes < 59 -- finish
  )
select minutesOfHour.minutes 
from minutesOfHour
--where minutesOfHour.minutes in (10, 22) 
order by minutesOfHour.minutes