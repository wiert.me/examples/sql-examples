with 
  recursive 
  weekdaysOfWeek(weekdays) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select weekdaysOfWeek.weekdays + 1
    from weekdaysOfWeek
    where weekdaysOfWeek.weekdays < 6 -- finish
  )
select weekdaysOfWeek.weekdays 
from weekdaysOfWeek
--where weekdaysOfWeek.weekdays in (10, 22) 
order by weekdaysOfWeek.weekdays