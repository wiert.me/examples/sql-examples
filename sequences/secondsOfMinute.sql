with 
  recursive 
  secondsOfMinute(seconds) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select secondsOfMinute.seconds + 1
    from secondsOfMinute
    where secondsOfMinute.seconds < 59 -- finish
  )
select secondsOfMinute.seconds 
from secondsOfMinute
--where secondsOfMinute.seconds in (10, 22) 
order by secondsOfMinute.seconds