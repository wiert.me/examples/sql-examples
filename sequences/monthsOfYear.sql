with 
  recursive 
  monthsOfYear(months) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select monthsOfYear.months + 1
    from monthsOfYear
    where monthsOfYear.months < 12 -- finish
  )
select monthsOfYear.months 
from monthsOfYear
--where monthsOfYear.months in (10, 22) 
order by monthsOfYear.months