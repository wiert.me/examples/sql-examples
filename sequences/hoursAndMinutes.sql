with 
  recursive 
  hoursOfDay(hours) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select hoursOfDay.hours + 1
    from hoursOfDay
    where hoursOfDay.hours < 23 -- finish
  ),
  minutesOfHour(minutes) as (        
    -- When you select more than 1024 values, this error occurs:
    -- Error while fetching data:  Too many concurrent executions of the same request    
    select 0 -- start
    from rdb$database
    union all
    select minutesOfHour.minutes + 1
    from minutesOfHour
    where minutesOfHour.minutes < 59 -- finish
  )
select right('00' || cast(hoursOfDay.Hours as varchar(2)),2) 
        || ':' || 
        right('00' || cast(minutesOfHour.minutes as varchar(2)),2)
from minutesOfHour
join hoursOfDay on 1=1
--where hoursOfDay.hours in (10, 22) 
order by hoursOfDay.hours, minutesOfHour.minutes